addi $0 $ZERO 0     # $0 = 0
addi $1 $ZERO 0     # $1 = 0

beq $1 $0 2	        # [START OF LOOP] if $0 == $1 then skip loop

addi $1 $1 1        # $1++

jmp $PC -3		    # [END OF LOOP] jump to [START OF LOOP]

addi $0 $0 1        # $0++

jmp $ZERO 1         # Jump back to the start