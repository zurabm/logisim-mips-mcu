smem $ZERO $ZERO DDRD   # M[DDRD] = 0 NOTE: Set all pins in port D to output.

addi $0 $ZERO 32        # $0 = 32
addi $1 $ZERO 1         # $1 = 1
addi $2 $ZERO 0         # $0 = 0

beq $2 $0 -2            # [IF BRANCH] if $2 == $0, set $0 to 0

shl $3 $1 $2            # $3 = $1 << $2 (1 << $2) 
smem $ZERO $3 PORTD     # M[PORTD] = $3 (Set bit at index $2)

addi $2 $2 1            # $2++
jmp $PC -5              # [INFINITE LOOP] jump to [IF BRANCH] 