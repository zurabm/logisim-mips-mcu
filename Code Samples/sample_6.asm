smem $ZERO $ZERO DDRA   # M[DDRA] = 0 NOTE: Set all pins in port A to output.
smem $ZERO $ZERO DDRB   # M[DDRB] = 0 NOTE: Set all pins in port B to output.
smem $ZERO $ZERO DDRC   # M[DDRC] = 0 NOTE: Set all pins in port C to output.
smem $ZERO $ZERO DDRD   # M[DDRD] = 0 NOTE: Set all pins in port D to output.

addi $0 $ZERO 21845     # $0 = 0b0000000000000000:0101010101010101
shli $0 $0 16           # $0 = 0b0101010101010101:0000000000000000
addi $0 $0 21845        # $0 = 0b0101010101010101:0101010101010101

bwxori $0 $0 -1         # $0 = 0b1010101010101010:1010101010101010 | And flips [START OF LOOP]
bwxori $1 $0 -1         # $1 = 0b0101010101010101:0101010101010101 | And flips

smem $ZERO $0 PORTA     # M[PORTA] = $0 (Set every other bit in PORTA)
smem $ZERO $1 PORTB     # M[PORTB] = $1 (Set every other bit in PORTB)
smem $ZERO $0 PORTC     # M[PORTC] = $0 (Set every other bit in PORTC)
smem $ZERO $1 PORTD     # M[PORTD] = $1 (Set every other bit in PORTD)

jmp $PC -7              # [INFINITE LOOP] jump to [START OF LOOP] 