# MCU Instruction Set

## Note
This is a 32-bit MCU. All instructions, words, ports and registers are 32 bits wide.

## Arithmetic Instructions
| Instruction | Usage Example    | Meaning        | Description                                                                                     |
| ----------- | ---------------- | -------------- | ----------------------------------------------------------------------------------------------- |
| add         | add $1 $2 $3     | $1 = $2+$3     | Adds the values stored in register $2 and $3 and stores the result in register $1.              |
| addi        | addi $1 $2 100   | $1 = $2+100    | Adds an immediate value (100) to the value in register $2 and stores the result in register $1. |
| sub         | sub $1 $2 $3     | $1 = $2-$3     | Subtracts the value in $3 from $2 and stores the result in the register $1.                     |
| subi        | subi $1 $2 50    | $1 = $2-50     | Subtracts the immediate value (50) from the value in $2 and stores the result in register $1.   |
| div         | div $1 $2 $3     | $1 = $2/$3     | Stores the result of the division of the value in $2 by $3 in register $1.                      |
| divi        | div $1 $2 5      | $1 = $2/5      | Same as div but the right hand value is immediate (5 in this case).                             |
| mul	      | mul $1 $2 $3     | $1 = $2*$3     | Multiplication operation.                                                                       |
| muli	      | mul $1 $2 10     | $1 = $2*10     | Multiplication with an immediate value.                                                         |
| shl         | shl $1 $2 $3     | $1 = $2 << $3  | (Arithmetical) Left Shift the value stored in $2 by $3 digits and stores the result in $1.      |
| shli        | shli $1 $2 5     | $1 = $2 << $3  | Same as 'shl' but with an immediate value.                                                      |
| shr         | shr $1 $2 $3     | $1 = $2 >> $3  | (Arithmetical) Right Shift the value stored in $2 by $3 digits and stores the result in $1.     |
| shri        | shri $1 $2 5     | $1 = $2 >> $3  | Same as 'shr' but with an immediate value.                                                      |
| eq          | eq $1 $2 $3      | eq $1 = $2 $3  | Store Result of $2 == $3 in $1.                                                                 |   
| eqi         | eqi $1 $2 5      | eqi $1 = $2 5  | Store Result of $2 == 5 in $1.                                                                  |   
| neq         | neq $1 $2 $3     | neq $1 = $2 $3 | Store Result of $2 != $3 in $1.                                                                 |   
| neqi        | neqi $1 $2 5     | neqi $1 = $2 5 | Store Result of $2 != 5 in $1.                                                                  |   
| lt          | lt $1 $2 $3      | lt $1 = $2 $3  | Store Result of $2 < $3 in $1.                                                                  |   
| lti         | lti $1 $2 5      | lti $1 = $2 5  | Store Result of $2 < 5 in $1.                                                                   |   
| gt          | gt $1 $2 $3      | gt $1 = $2 $3  | Store Result of $2 > $3 in $1.                                                                  |   
| gti         | gti $1 $2 5      | gti $1 = $2 5  | Store Result of $2 > 5 in $1.                                                                   |   
| le          | le $1 $2 $3      | le $1 = $2 $3  | Store Result of $2 <= $3 in $1.                                                                 |   
| lei         | lei $1 $2 5      | lei $1 = $2 5  | Store Result of $2 <= 5 in $1.                                                                  |   
| ge          | ge $1 $2 $3      | ge $1 = $2 $3  | Store Result of $2 >= $3 in $1.                                                                 |   
| gei         | gei $1 $2 5      | gei $1 = $2 5  | Store Result of $2 >= 5 in $1.                                                                  |   

## Bit-wise Instructions
| Instruction | Usage Example    | Meaning        | Description                                                                                     |
| ----------- | ---------------- | -------------- | ----------------------------------------------------------------------------------------------- |
| bwor        | bwor $1 $2 $3    | $1 = $2\|$3    | Stores the result of the bit-wise OR operation between the values in $2 and $3 in register $1.  |
| bwori       | bwori $1 $2 5    | $1 = $2\|5     | Stores the result of the bit-wise OR operation between the value in $2 and 5 in register $1.    |
| bwand       | bwand $1 $2 $3   | $1 = $2&$3     | Stores the result of the bit-wise AND operation between the values in $2 and $3 in register $1. |
| bwandi      | bwandi $1 $2 5   | $1 = $2&5      | Stores the result of the bit-wise AND operation between the value in $2 and 5   in register $1. |
| bwnot       | bwnot $1 $2      | $1 = ~$2       | Stores the result of the bit-wise NOT operation on the value in $2 in register $1.              |
| bwnoti      | bwnoti $1 5      | $1 = ~5        | Stores the result of the bit-wise NOT operation on the value 5 in register $1.                  |
| bwxor       | bwxor $1 $2 5    | $1 = $2^$3     | Stores the result of the bit-wise XOR operation between the values in $2 and $3 in register $1. |
| bwxori      | bwxori $1 $2 5   | $1 = $2^5      | Stores the result of the bit-wise XOR operation between the value in $2 and 5 in register $1.   |

## Memory Instructions
| Instruction | Usage Example    | Meaning        | Description                                                                                     |
| ----------- | ---------------- | -------------- | ----------------------------------------------------------------------------------------------- |
| lmem        | lmem $1 $2 5     | $1 = Mem[$2+5] | Loads the value stored at memory address $2 + 5 into register $1.                               |
| smem        | smem $1 $2 5     | Mem[$1+5] = $2 | Stores the value in register $2 in the memory location at address $1 + 5.                       |

## Jump Instructions
| Instruction | Usage Example    | Meaning        | Description                                                                                     |
| ----------- | ---------------- | -------------- | ----------------------------------------------------------------------------------------------- |
| jmp         | jmp $1 5         | Jump to $1 + 5 | Jumps to the instruction at memory address $1 + 5, i.e. load $1 + 5 into the program counter.   |
| beq         | beq $1 $2 5      | Jump to PC + 5 | Jump to  Jump to instruction at memory address PC + 10 if the value stored in $1 equals the one |
|             |                  | if $1 == $2    | stored in $2.                                                                                   |
| bneq        | beq $1 $2 5      | Jump to PC + 5 | Jump to instruction at memory address PC + 10 if the value stored in $1 does not equal the one  |
|             |                  | if $1 != $2    | stored in $2.                                                                                   |
| blt         | beq $1 $2 5      | Jump to PC + 5 | Jump to instruction at memory address PC + 10 if the value stored in $1 is less than the one    |
|             |                  |  if $1 < $2    | stored in $2.                                                                                   |
| bgt         | beq $1 $2 5      | Jump to PC + 5 | Jump to instruction at memory address PC + 10 if the value stored in $1 is greater than the one |
|             |                  | if $1 > $2     | stored in $2.                                                                                   |
| ble         | beq $1 $2 5      | Jump to PC + 5 | Jump to instruction at memory address PC + 10 if the value stored in $1 is less than or equal   | 
|             |                  | if $1 <= $2    | to the one in $2.                                                                               |
| bge         | beq $1 $2 5      | Jump to PC + 5 | Jump to instruction at memory address PC + 10 if the value stored in $1 is greater than or      | 
|             |                  | if $1 >= $2    | equal to the one in $2.                                                                         |
___


# Instruction Format

## Type I

___
### Format for Arithmetic and Bit-wise instructions **without** immediate values.
| Instruction Type - 2 Bits | Operation Code - 6 Bits | Destination Register - 4 Bits | Left Source Register - 4 Bits | Right Source Register - 4 Bits | Unused - 12 Bits |
| ------------------------- | ----------------------- | ----------------------------- | ----------------------------- | ------------------------------ | ---------------- |
| Specifies the format and type of the instruction | Specifies the operation within the family of instructions | Register to load into | First source register address | Second source register address | Not used for this type of instruction |
___
### Format for Memory instructions.
| Instruction Type - 2 Bits | Operation Code - 6 Bits | Destination Register - 4 Bits | Left Source Register - 4 Bits | Right Source Register - 4 Bits | Immediate Value - 12 Bits |
| ------------------------- | ----------------------- | ----------------------------- | ----------------------------- | ------------------------------ | ------------------------- |
| Specifies the format and type of the instruction | Specifies the operation within the family of instructions | Register to load into if it's a load operation | Register containing the address | Register with the value to be stored if it's a store operation | The immediate (literal) value used in the instruction |
## Type II

___
### Format for Arithmetic and Bit-wise instructions **with** immediate values.
| Instruction Type - 2 Bits | Operation Code - 6 Bits | Destination Register - 4 Bits | Left Source Register - 4 Bits | Immediate Value - 16 Bits |
| ------------------------- | ----------------------- | ----------------------------- | ----------------------------- | ------------------------- |
| Specifies the format and type of the instruction | Specifies the operation within the family of instructions | Register to load into | Source register | The immediate (literal) value used in the instruction |

## Type III

___
### Format for **conditional** Jump Instructions.
| Instruction Type - 2 Bits | Operation Code - 6 Bits | Left Source Register - 4 Bits | Right Source Register - 4 Bits  | Immediate Value - 16 Bits |
| ------------------------- | ----------------------- | ----------------------------- | ------------------------------- | ------------------------- |
| Specifies the format and type of the instruction | Specifies the operation within the family of instructions | Register on the left side of the comparison | Register on the right side | The immediate (literal) value used in the instruction |

___
## Type IV 

___
### Format for **unconditional** Jump Instructions
| Instruction Type - 2 Bits | Operation Code - 6 Bits | Left Source Register - 4 Bits | Immediate Value - 20 Bits | 
| ------------------------- | ----------------------- | ----------------------------- | ------------------------- |
| Specifies the format and type of the instruction | Specifies the operation within the family of instructions | Register to read a value from | The immediate (literal) value used in the instruction |

___

## Operation Code

The operation code consists of six contiguous bits.

| Memory Operation Flag - 1 Bit                    | Memory Operation Type - 1 Bit                                        | ALU Operation Code - 4 Bits    |
| ------------------------------------------------ | -------------------------------------------------------------------- | ------------------------------ |
| If 1 the instruction contains a memory operation | If 1 the instruction is a store operation, else it loads from memory | The Operation code for the ALU |

# Register Descriptions

| Register Name             | Reference | Address | Purpose                                            |
| ------------------------- | --------- | ------- | -------------------------------------------------- |
| Zero Register             | $ZERO     | 0x0     | Store the value zero. Read-only register           |
| Program Counter Register  | $PC       | 0x1     | Stores the current instruction address             |
| Stack Pointer Register    | $SP       | 0x2     | Stores the current address of the top of the stack |
| General Purpose Registers | $0-$12    | 0x3-0xf | Registers you can use for any purpose              |

___

# Special Function Registers

| Register Name | Address  | Purpose                                                                                                                  |
| ------------- | -------- | ------------------------------------------------------------------------------------------------------------------------ |
| PORTA         | 0x000000 | Contains Input/Output data for pins in Port A.                                                                           |
| DDRA          | 0x000001 | Data Direction Register for Port A, 0 -> Output, 1 -> Input.                                                             |
| PINA          | 0x000002 | Pull Mode Register for Port A, 0 -> No-pull, 1 -> Internal Pull-up. If pin is set to output this register has no effect. |
| PORTB         | 0x000003 | Contains Input/Output data for pins in Port B.                                                                           |
| DDRB          | 0x000004 | Data Direction Register for Port B, 0 -> Output, 1 -> Input.                                                             |
| PINB          | 0x000005 | Pull Mode Register for Port B, 0 -> No-pull, 1 -> Internal Pull-up. If pin is set to output this register has no effect. |
| PORTC         | 0x000006 | Contains Input/Output data for pins in Port C.                                                                           |
| DDRC          | 0x000007 | Data Direction Register for Port C, 0 -> Output, 1 -> Input.                                                             |
| PINC          | 0x000008 | Pull Mode Register for Port C, 0 -> No-pull, 1 -> Internal Pull-up. If pin is set to output this register has no effect. |
| PORTD         | 0x000009 | Contains Input/Output data for pins in Port D.                                                                           |
| DDRD          | 0x00000a | Data Direction Register for Port D, 0 -> Output, 1 -> Input.                                                             |
| PIND          | 0x00000b | Pull Mode Register for Port D, 0 -> No-pull, 1 -> Internal Pull-up. If pin is set to output this register has no effect. |

___

# Instruction Encoding

## Symbol Legend
* 0, 1 - Actual Bit Values.
* X - Unused Bit.
* D - Destination Register Address Bit.
* L - Left Source Register Address Bit.
* R - Right Source Register Address Bit.
* I - Immediate Value Bit.

| Instruction | Encoding                              |
| ----------- |:------------------------------------- |
| add         | 00:0X0000:DDDD:LLLL:RRRR:XXXXXXXXXXXX | 
| sub         | 00:0X0001:DDDD:LLLL:RRRR:XXXXXXXXXXXX | 
| div         | 00:0X0010:DDDD:LLLL:RRRR:XXXXXXXXXXXX | 
| mul         | 00:0X0011:DDDD:LLLL:RRRR:XXXXXXXXXXXX |
| shl         | 00:0X0100:DDDD:LLLL:RRRR:XXXXXXXXXXXX | 
| shr         | 00:0X0101:DDDD:LLLL:RRRR:XXXXXXXXXXXX | 
| bwor        | 00:0X0110:DDDD:LLLL:RRRR:XXXXXXXXXXXX |
| bwand       | 00:0X0111:DDDD:LLLL:RRRR:XXXXXXXXXXXX |
| bwnot       | 00:0X1000:DDDD:XXXX:RRRR:XXXXXXXXXXXX |
| bwxor       | 00:0X1001:DDDD:LLLL:RRRR:XXXXXXXXXXXX |
| eq          | 00:0X1010:DDDD:LLLL:RRRR:XXXXXXXXXXXX |
| neq         | 00:0X1011:DDDD:LLLL:RRRR:XXXXXXXXXXXX |
| lt          | 00:0X1100:DDDD:LLLL:RRRR:XXXXXXXXXXXX |
| gt          | 00:0X1101:DDDD:LLLL:RRRR:XXXXXXXXXXXX |
| le          | 00:0X1110:DDDD:LLLL:RRRR:XXXXXXXXXXXX |
| ge          | 00:0X1111:DDDD:LLLL:RRRR:XXXXXXXXXXXX |
| lmem        | 00:100000:DDDD:LLLL:XXXX:IIIIIIIIIIII |
| smem        | 00:110000:XXXX:LLLL:RRRR:IIIIIIIIIIII |
|             |                                       |
| addi        | 01:0X0000:DDDD:LLLL:IIII:IIIIIIIIIIII | 
| subi        | 01:0X0001:DDDD:LLLL:IIII:IIIIIIIIIIII |
| divi        | 01:0X0010:DDDD:LLLL:IIII:IIIIIIIIIIII |
| muli        | 01:0X0011:DDDD:LLLL:IIII:IIIIIIIIIIII |
| shli        | 01:0X0100:DDDD:LLLL:IIII:IIIIIIIIIIII |
| shri        | 01:0X0101:DDDD:LLLL:IIII:IIIIIIIIIIII |
| bwori       | 01:0X0110:DDDD:LLLL:IIII:IIIIIIIIIIII |
| bwandi      | 01:0X0111:DDDD:LLLL:IIII:IIIIIIIIIIII |
| bwnoti      | 01:0X1000:DDDD:XXXX:IIII:IIIIIIIIIIII |
| bwxori      | 01:0X1001:DDDD:LLLL:IIII:IIIIIIIIIIII |
| eqi         | 01:0X1010:DDDD:LLLL:IIII:IIIIIIIIIIII |
| neqi        | 01:0X1011:DDDD:LLLL:IIII:IIIIIIIIIIII |
| lti         | 01:0X1100:DDDD:LLLL:IIII:IIIIIIIIIIII |
| gti         | 01:0X1101:DDDD:LLLL:IIII:IIIIIIIIIIII |
| lei         | 01:0X1110:DDDD:LLLL:IIII:IIIIIIIIIIII |
| gei         | 01:0X1111:DDDD:LLLL:IIII:IIIIIIIIIIII |
|             |                                       |
| beq         | 10:0X1010:LLLL:RRRR:IIII:IIIIIIIIIIII |
| bneq        | 10:0X1011:LLLL:RRRR:IIII:IIIIIIIIIIII |
| blt         | 10:0X1100:LLLL:RRRR:IIII:IIIIIIIIIIII |  
| bgt         | 10:0X1101:LLLL:RRRR:IIII:IIIIIIIIIIII | 
| ble         | 10:0X1110:LLLL:RRRR:IIII:IIIIIIIIIIII |  
| bge         | 10:0X1111:LLLL:RRRR:IIII:IIIIIIIIIIII |
|             |                                       |
| jmp         | 11:0X0000:LLLL:IIII:IIII:IIIIIIIIIIII |