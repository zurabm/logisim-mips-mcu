# MCU
## ქართულად

___

ამ დირექტორიაში შეგიძლია ნახოთ შემდეგი ფაილები:
1. MCU.circ - Logisim-ის გარემოში აწყობილი MIPS-ის მსგავსი არქიტექტურის პროცესორი და მიკროკონტროლერი.
1. MCU Instruction Set - პროცესორის მხარდაჭერილი ინსტრუქციების, რეგისტრების განლაგების და სხვა დეტალების მოკლე აღწერა.
1. Code Samples-ს დირექტორიაში მოთავსებული ფაილები - ასემბლის კოდების და მათი კომპილირებული ვერსიების მაგალითები.
1. assembly - პითონზე დაწერილი ასემბლის კომპილატორი.

___

### კომპილატორის გამოსაყენებლად დამატებითი ინსტრუქციები იხილეთ ფაილში ./assembly/start.py.

## English

___

This repository contains the following files:
1. MCU.circ - The MIPS MCU built using Logisim's simulated environment.
1. MCU Instruction Set - A description of the instruction set supported by the processors, as well as information on the existing SFRs and some other details.
1. Files in the Code Samples directory - Examples of assembly code snippets and their compiled versions.
1. assembly - A simple assembly compiler written in Python.

___

### For additional informatin on how to use the compiler check out the instructions in the file ./assembly/start.py.
