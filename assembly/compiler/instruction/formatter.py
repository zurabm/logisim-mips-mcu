"""A module used for generating formatted MCU instructions from a specified format
string and a list of arguments."""


import sys
sys.path.extend(['.', '..', '../..', '../../..'])


import logging
from string import Formatter
from typing import Iterable

import assembly.compiler.loggers as loggers

logger = loggers.get_logger(__name__, logging.WARNING)

UNUSED = 'U'
REGISTER = 'R'
IMMEDIATE = 'I'

FORMAT_1 = lambda header: f'{header}{{0!{REGISTER}:4}}{{1!{REGISTER}:4}}{{2!{REGISTER}:4}}{{!{UNUSED}:12}}'
FORMAT_2 = lambda header: f'{header}{{0!{REGISTER}:4}}{{!{UNUSED}:4}}{{1!{REGISTER}:4}}{{!{UNUSED}:12}}'
FORMAT_3 = lambda header: f'{header}{{0!{REGISTER}:4}}{{1!{REGISTER}:4}}{{!{UNUSED}:4}}{{2!{IMMEDIATE}:12}}'
FORMAT_4 = lambda header: f'{header}{{!{UNUSED}:4}}{{0!{REGISTER}:4}}{{1!{REGISTER}:4}}{{2!{IMMEDIATE}:12}}'
FORMAT_5 = lambda header: f'{header}{{0!{REGISTER}:4}}{{1!{REGISTER}:4}}{{2!{IMMEDIATE}:16}}'
FORMAT_6 = lambda header: f'{header}{{0!{REGISTER}:4}}{{!{UNUSED}:4}}{{1!{IMMEDIATE}:16}}'
FORMAT_7 = lambda header: f'{header}{{0!{REGISTER}:4}}{{1!{IMMEDIATE}:20}}'

INSTRUCTION_FORMATS: 'dict[str, str]' = {
    'add':          FORMAT_1('00000000'),
    'sub':          FORMAT_1('00000001'),
    'div':          FORMAT_1('00000010'),
    'mul':          FORMAT_1('00000011'),
    'shl':          FORMAT_1('00000100'),
    'shr':          FORMAT_1('00000101'),
    'bwor':         FORMAT_1('00000110'),
    'bwand':        FORMAT_1('00000111'),
    'bwnot':        FORMAT_2('00001000'),
    'bwxor':        FORMAT_1('00001001'),
    'eq':           FORMAT_1('00001010'),
    'neq':          FORMAT_1('00001011'),
    'lt':           FORMAT_1('00001100'),
    'gt':           FORMAT_1('00001101'),
    'le':           FORMAT_1('00001110'),
    'ge':           FORMAT_1('00001111'),
    'lmem':         FORMAT_3('00100000'),
    'smem':         FORMAT_4('00110000'),

    'addi':         FORMAT_5('01000000'),
    'subi':         FORMAT_5('01000001'),
    'divi':         FORMAT_5('01000010'),
    'muli':         FORMAT_5('01000011'),
    'shli':         FORMAT_5('01000100'),
    'shri':         FORMAT_5('01000101'),
    'bwori':        FORMAT_5('01000110'),
    'bwandi':       FORMAT_5('01000111'),
    'bwnoti':       FORMAT_6('01001000'),
    'bwxori':       FORMAT_5('01001001'),
    'eqi':          FORMAT_5('01001010'),
    'neqi':         FORMAT_5('01001011'),
    'lti':          FORMAT_5('01001100'),
    'gti':          FORMAT_5('01001101'),
    'lei':          FORMAT_5('01001110'),
    'gei':          FORMAT_5('01001111'),

    'beq':          FORMAT_5('10001010'),
    'bneq':         FORMAT_5('10001011'),
    'blt':          FORMAT_5('10001100'),
    'bgt':          FORMAT_5('10001101'),
    'ble':          FORMAT_5('10001110'),
    'bge':          FORMAT_5('10001111'),

    'jmp':          FORMAT_7('11000000'),     
}

REGISTER_PREFIX = '$'

N_GENERAL_PURPOSE_REGISTERS = 13

REGISTER_MAP: 'dict[str, int]' = {    

    f'{REGISTER_PREFIX}ZERO': 0x0,
    f'{REGISTER_PREFIX}PC': 0x1,
    f'{REGISTER_PREFIX}SP': 0x2,
    **dict((f'{REGISTER_PREFIX}{i}', 0x3 + i) for i in range(N_GENERAL_PURPOSE_REGISTERS)),
}

PORTS = ['A', 'B', 'C', 'D']

SPECIAL_FUNCTION_REGISTER_MAP: 'dict[str, int]' = {

    **dict((f'PORT{PORT_NAME}', PORT_INDEX * 3 + 0x0) for PORT_INDEX, PORT_NAME in enumerate(PORTS)),
    **dict((f'DDR{PORT_NAME}', PORT_INDEX * 3 + 0x1) for PORT_INDEX, PORT_NAME in enumerate(PORTS)),
    **dict((f'PIN{PORT_NAME}', PORT_INDEX * 3 + 0x2) for PORT_INDEX, PORT_NAME in enumerate(PORTS)),
}


def format_instruction(instruction_type: str, *arguments: str) -> str:
    """Generates a formated binary instruction from the specified
    instruction type and arguments.
    
    ### Parameters
    
    * @instruction_type - The type of the instruction to be generated.
    * @arguments - The list of arguments for the instruction.
    
    @return The binary instruction as a string."""

    if instruction_type not in INSTRUCTION_FORMATS:
        raise ValueError(f'{instruction_type} is not a supported instruction.')

    result = []
    format_string = INSTRUCTION_FORMATS[instruction_type]

    used_arguments = set()

    for leading_text, arg_position, n_bits_occupied, arg_type in Formatter().parse(format_string):
        
        result.append(leading_text)
        if arg_type is None:
            break

        n_bits_occupied = int(n_bits_occupied)

        if arg_type == UNUSED:
            value = 0
        else:

            arg_position = int(arg_position)
            if arg_position >= len(arguments):
                raise ValueError(f'The instruction {instruction_type} is missing a positional '
                                 f'argument at index {arg_position}.')
            
            used_arguments.add(arg_position)

            if arg_type == REGISTER:

                register_name = arguments[arg_position]

                if not register_name.startswith(REGISTER_PREFIX):
                    raise ValueError(f'The argument at index {arg_position} for the instruction {instruction_type} '
                                    f'must be a register name beginning with the prefix "{REGISTER_PREFIX}"".')

                if register_name not in REGISTER_MAP:
                    raise ValueError(f'{register_name} is not a valid Quick Access register for the MCU.\n'
                                    f'Valid registers are: {", ".join(REGISTER_MAP.keys())}')
                
                value = REGISTER_MAP[register_name]

            elif arg_type == IMMEDIATE:

                immediate_value = arguments[arg_position]

                if not __is_int(immediate_value) and immediate_value not in SPECIAL_FUNCTION_REGISTER_MAP: 

                    sfr_string_rep = "\n\t".join(f"{NAME}: {ADDRESS}" for NAME, ADDRESS in SPECIAL_FUNCTION_REGISTER_MAP.items())

                    raise ValueError(f'The argument at index {arg_position} for the instruction {instruction_type} '
                                     f'must be an integer immediate value or the name of a special function register '
                                     f'and not "{immediate_value}".\n'
                                     f'List of special function registers:\n'
                                     f'\t{sfr_string_rep}')

                value = SPECIAL_FUNCTION_REGISTER_MAP[immediate_value] if immediate_value in SPECIAL_FUNCTION_REGISTER_MAP \
                                                                       else int(immediate_value)  

        result.append(__get_bits(value, n_bits_occupied, unsigned=(arg_type==REGISTER)))

    if len(used_arguments) != len(arguments):
        raise ValueError(f'The instruction {instruction_type} only uses {len(used_arguments)} arguments, '
                         f'but {len(arguments)} were provided.')

    return ''.join(result)

def __is_int(value: str) -> bool:
    """Returns true if the specified value is a parsable integer."""

    return value.isdigit() or ((value.startswith('-') or value.startswith('+')) and value[1:].isdigit())

def __get_bits(value: int, n_bits: int, unsigned: bool=False) -> str:
    """Returns the binary representation of the specified value.
    
    ### Parameters
    
    * @value - The value to be converted to binary.
    * @n_bits - The number of bits the binary representation should be padded/truncated to.
    * @unsigned - If false a warning will be logged when the MSB and the sign of the value don't match.
    
    @return The binary representation of the value as a string."""

    if value >= 0:
        binary_value = bin(value)[2:].zfill(n_bits)
    else:
        binary_value = bin((-value ^ int('1' * n_bits, 2)) + 1)[2:]

    expected_sign = '0' if value >= 0 else '1'

    if len(binary_value) > n_bits or (not unsigned and len(binary_value) == n_bits and binary_value[0] != expected_sign):
        logger.warning(f'Overflow when converting the value {value} into binary with {n_bits} bits.'
                       f'Value in binary: {binary_value} - Truncated: {binary_value[-n_bits:]}')

    return binary_value[-n_bits:]

# === Internal validations ----------------------- #
 
def __validate_format_strings(format_strings: Iterable[str]) -> None:
    """Validates the specified format strings for inconsistencies."""

    try:
        for format_string in format_strings:
            for _, arg_position, n_bits_occupied, arg_type in Formatter().parse(format_string):
                
                assert arg_type is not None or arg_position is None, \
                f'Argument type must be specified for the argument at position {arg_position}.'
            
                assert arg_type in {UNUSED, REGISTER, IMMEDIATE}, \
                f'{arg_type} is not a valid argument type.'

                assert n_bits_occupied is not None, \
                f'The number of bits occupied by '  \
                f'{UNUSED if arg_type == UNUSED else f"the argument at position {arg_position}"} must be specified.' 
                
                assert n_bits_occupied.isdigit(), \
                f'The bits occupied by an argument must be an integer and not {n_bits_occupied}.'

                if arg_type == UNUSED:
                    if arg_position != '':
                        logger.warning(f'Argument position {arg_position} is specified for an argument '
                                       f'with type "{arg_type}".')
                
                elif arg_type in {REGISTER, IMMEDIATE}:
                    assert arg_position.isdigit(), \
                    f'Argument position must be ah integer for the argument type "{arg_type}"'

    except AssertionError as error:

        print(f'An assertion error was raise for format string: {format_string}\n'
              f'Error: {error}\n'
              f'Traceback: {error.__traceback__}')

        raise SystemExit()


# Validation Execution.
__validate_format_strings(INSTRUCTION_FORMATS.values())