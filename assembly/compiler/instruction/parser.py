"""Module used for parsing assembly instructions for the MCU."""


import sys
sys.path.extend(['.', '..', '../..', '../../..'])


import logging
from typing import Tuple

import assembly.compiler.loggers as loggers

logger = loggers.get_logger(__name__, logging.WARNING)

COMMENT_SYMBOL = '#'

def parse_line(line: str) -> Tuple[str or None, 'list[str]']:
    """Parses the specified line of assembly code.
    
    ### Parameters
    
    * @line - The line to be parsed as a string.
    * @register_map - A dictionary specifying the addresses of named registers.
    
    @return - The instruction and the list of arguments as a tuple."""

    assembly_instruction = (line[:line.index(COMMENT_SYMBOL)] if COMMENT_SYMBOL in line else line).strip()

    tokens = list(filter(lambda token: token != '', assembly_instruction.split(' ')))
    
    instruction = tokens[0] if len(tokens) > 0 else None
    arguments = tokens[1:]

    return instruction, arguments