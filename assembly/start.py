"""This is the entry point script for the assembly compiler.

The compiler requires two positional parameters:
1. The path to the assembly file to be compiled.
2. The path to the output file.

For now the compiler doesn't support any other generation options.

Example command for Windows (Current working directory is set to be the root of this project):
    $ python3 ./assembly/start.py './Code Samples/sample_3.asm' './Code Samples/sample_3.hex'

Where:
    python3 - Your python interpreter.
    ./assembly/start.py - Path to this python script.
    './Code Samples/sample_3.asm' - Path to the text file with the assembly instructions.
    './Code Samples/sample_3.hex' - Path to the output file (The file itself doesn't have to exist).
"""


import sys
sys.path.extend(['.', '..', '../..', '../../..'])

import logging

import assembly.compiler.instruction.formatter as formatter
import assembly.compiler.instruction.parser as parser
import assembly.compiler.bytecode.writer as writer

import assembly.compiler.loggers as loggers

logger = loggers.get_logger('core', logging.WARNING)

OUTPUT_FORMAT = writer.OutputFormat.HEX
PACKING = writer.Packing.WORDS
ADDRESSING = writer.Addressing.PLAIN

SOURCE_FILE = 1
DESTINATION_FILE = 2

def compile_code(source_file_name: str, output_file_name: str) -> None:

    bytecodes: list[str] = []

    with open(source_file_name) as assembly_file:
        for line_number, line in enumerate(assembly_file, start=1):
            
            instruction, arguments = parser.parse_line(line)
            
            if instruction is not None:
                
                try:
                    bytecodes.append(formatter.format_instruction(instruction, *arguments))
                except ValueError as error:

                    print(f'Error parsing line {line_number}: {line}\n'
                          f'Error: {error}')          

                    raise SystemExit()

    writer.generate_bytecode(bytecodes, output_file_name, OUTPUT_FORMAT, PACKING, ADDRESSING)


if __name__ == '__main__':

    compile_code(sys.argv[SOURCE_FILE], sys.argv[DESTINATION_FILE])