import pytest
import csv
import sys

from typing import Union

sys.path.extend(['.', '..', '../..', '../../..'])

TEST_DATA_FILE = './assembly/tests/resources/test_data.csv'

COLUMN_LINE = 'Line'
COLUMN_INSTRUCTION = 'Instruction'
COLUMN_ARGUMENTS = 'Arguments'
COLUMN_FORMATTED_LINE = 'Formatted Line'
COLUMN_OUTPUT = 'Output'

USED_COLUMNS = frozenset((
    COLUMN_LINE,
    COLUMN_INSTRUCTION,
    COLUMN_ARGUMENTS,
    COLUMN_FORMATTED_LINE,
    COLUMN_OUTPUT,
))

columns: 'list[str]' = None
test_data: 'dict[str, list[str]]' = None

def read_test_data():

    global columns
    global test_data

    with open(TEST_DATA_FILE, newline='') as csvfile:

        csv_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        rows = list(csv_reader)

        columns = rows[0]

        data_columns = set(columns)
        if len(USED_COLUMNS - data_columns) != 0:
            raise ValueError(f'The test data columns do not match.\n'
                             f'Missing columns: {", ".join(USED_COLUMNS - data_columns)}\n'
                             f'Unexpected columns: {", ".join(data_columns - USED_COLUMNS)}\n')     

        test_data = dict((column_name, []) for column_name in columns if column_name in USED_COLUMNS)

        for row in rows[1:]:
            for column_index, value in enumerate(row):
                
                column_name = columns[column_index]

                if column_name in test_data:
                    test_data[column_name].append(value)

def load_data(fixture_function):
    
    def wrapper(*args, **kwargs):
        
        if test_data is None:
            read_test_data()

        return fixture_function(*args, **kwargs)
    
    return wrapper

@pytest.fixture(scope='module')
@load_data
def test_lines() -> 'list[str]':
    return list(test_data[COLUMN_LINE])

@pytest.fixture(scope='module')
@load_data
def test_instructions() -> 'list[Union[str, None]]':
    return list(
        map(lambda instruction: None if instruction == '' else instruction,
        test_data[COLUMN_INSTRUCTION])
    )
    
@pytest.fixture(scope='module')
@load_data
def test_arguments() -> 'list[str]':
    return list(
        map(lambda arguments: [] if arguments == '' else arguments.strip().split(' '), 
        test_data[COLUMN_ARGUMENTS])
    )
    
@pytest.fixture(scope='module')
@load_data
def test_formatted_lines() -> 'list[str]':
    return list(
        map(lambda formatted_line: formatted_line.replace(':', ''),
        test_data[COLUMN_FORMATTED_LINE])
    )
    
@pytest.fixture(scope='module')
@load_data
def test_outputs() -> 'list[str]':
    return list(test_data[COLUMN_OUTPUT])
