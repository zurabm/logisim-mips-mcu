import logging

import assembly.compiler.instruction.parser as parser
import assembly.tests.loggers as loggers

from assembly.tests.fixtures import test_lines, test_instructions, test_arguments

logger = loggers.get_logger(__name__, logging.INFO)

def test_parse_line(test_lines, test_instructions, test_arguments):

    case_id = 1

    for test_line, instruction, arguments in zip(test_lines, test_instructions, test_arguments):

        logger.info(f'TC {case_id}:\n'
                    f'\tLine: {test_line}\n'
                    f'\tInstruction: {instruction}\n'
                    f'\tArguments: {arguments}\n')

        assert parser.parse_line(test_line) == (instruction, arguments)

        case_id += 1